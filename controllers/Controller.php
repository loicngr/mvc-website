<?php


namespace Controller;

include_once 'utils.php';
include_once 'views/View.php';

use Utils\Utils;
use View\View;

class Controller extends View implements Utils
{

    /**
     * Retourne le nom du fichier avec l'extension
     *
     * @param string $fileName
     * @return false|string
     */
    protected function getFile($fileName)
    {
        $fileName = htmlspecialchars($fileName) . '.php';

        if(!file_exists('views/' . $fileName)) return false;
        return "views/{$fileName}";
    }

    /**
     * Affiche une erreur dans le page
     *
     * @param string $msg
     */
    public function printError($msg): void
    {
        printf($msg);
        echo "<br><a href='/'>Back home</a>.";
        die();
    }
}