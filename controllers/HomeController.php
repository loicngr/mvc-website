<?php


namespace HomeController;

include_once 'Controller.php';
use Controller\Controller;

/**
 * Class HomeController
 * @package HomeController
 *
 * Contrôleur de la page Home
 */
class HomeController extends Controller
{
    /**
     * Affichage de la view
     *
     * @param string $viewName
     */
    public function render($viewName)
    {
        if (!isset($viewName) || empty($viewName)) $this->printError("View name is not defined.");

        $viewName = htmlspecialchars($viewName);

        $fileName = $this->getFile($viewName);
        if ($fileName) require_once $fileName;
    }
}