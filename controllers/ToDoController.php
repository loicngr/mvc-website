<?php


namespace ToDoController;

include_once 'Controller.php';
include_once 'models/ToDoModel.php';

use Controller\Controller;
use ToDoModel\ToDoModel;

class ToDoController extends Controller
{
    private $model;

    public function __construct()
    {
        $this->model  = new ToDoModel();
    }

    protected function getTodos(): array
    {
        return $this->model->getItems();
    }

    /**
     * Mise à jour d'un to do
     *
     * @param integer $id
     * @param string $name
     * @return bool
     */
    public function update($id, $name)
    {
        return $this->model->updateItemByID(intval($id), htmlspecialchars($name));
    }

    /**
     * Ajout d'un to do
     *
     * @param string $name
     * @return bool
     */
    public function add($name)
    {
        return $this->model->addItem(htmlspecialchars($name));
    }

    /**
     * Supprime un to do
     *
     * @param integer $id
     * @return bool
     */
    public function delete($id)
    {
        return $this->model->deleteItemByID(intval($id));
    }

    /**
     * Affichage de la view
     * @param $viewName
     * @param null $todoId
     */
    public function render($viewName, $todoId = null)
    {
        if (!isset($viewName) || empty($viewName)) $this->printError("View name is not defined.");

        $viewName = htmlspecialchars($viewName);

        switch ($viewName) {
            case 'todo/todos':
                $todosList = $this->model->getItems();
                break;
            case 'todo/todo':
                if (!isset($todoId) || empty($viewName)) $this->printError("ToDo Id not defined.");
                $todo = $this->model->getItemByID($todoId);
                break;
            default:
                $this->printError("Page not found.");
                break;
        }

        $fileName = $this->getFile($viewName);
        if ($fileName) require_once $fileName;
        else $this->printError("Page not found.");
    }
}