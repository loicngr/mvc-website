<?php

namespace App;

include_once 'router.php';

use Router\Router;

class App extends Router
{
    /**
     * Initialisation des routes
     */
    public function initRoutes(): void
    {
        $this->add_route('/', array($this, 'homeView'));

        $this->add_route('/todos', array($this, 'todosView'));

        $this->add_route('/todo', array($this, 'todoView'));

        $this->add_route('/todo/add', array($this, 'todoAddView'));

        $this->add_route('/todo/update', array($this, 'todoUpdateView'));

        $this->add_route('/todo/delete', array($this, 'todoDeleteView'));

        $this->addToDoByIDRoutes();

        $this->execute();
    }
}

$app = new App();
$app->initRoutes();
