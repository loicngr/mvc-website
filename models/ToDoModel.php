<?php

namespace ToDoModel;

include_once 'models/Model.php';

use DateTime;
use Model\Model;

class ToDoModel extends Model
{
    /**
     * Retourne tous les items de la base de données
     *
     * @return array
     */
    public function getItems(): array
    {
        $sql = "SELECT id, name, createdAt, updatedAt FROM items;";
        $request = $this->_PDO->prepare($sql);
        $request->execute();
        return $request->fetchAll();
    }

    /**
     * Retourne un item par son ID
     *
     * @param integer $id
     * @return object|bool
     */
    public function getItemByID($id)
    {
        $itemId = intval($id);

        $sql = "SELECT id, name, createdAt, updatedAt FROM items WHERE items.id = :id;";
        $request = $this->_PDO->prepare($sql);
        $request->bindParam(':id', $itemId);
        $request->execute();
        return $request->fetch();
    }

    /**
     * Supprime un item par son ID
     *
     * @param integer $id
     * @return bool
     */
    public function deleteItemByID($id): bool
    {
        $itemId = intval($id);

        $sql = "DELETE FROM items WHERE items.id = :id;";
        $request = $this->_PDO->prepare($sql);
        $request->bindParam(':id', $itemId);
        $request->execute();

        $isValid = $request->rowCount();

        if ($isValid) return true;
        return false;
    }

    /**
     * Met à jour un item par son ID
     *
     * @param integer $id
     * @param string $name
     * @return bool
     */
    public function updateItemByID($id, $name): bool
    {
        $itemId = intval($id);
        $itemName = htmlspecialchars($name);
        $date = new DateTime('now');
        $updateAtDate = $date->format('Y-m-d H:i:s');

        $sql = "UPDATE items SET items.name = :name, items.updatedAt = :updateAt WHERE items.id = :id";
        $request = $this->_PDO->prepare($sql);
        $request->bindParam(':id', $itemId);
        $request->bindParam(':name', $itemName);
        $request->bindParam(':updateAt', $updateAtDate);
        $request->execute();

        $isValid = $request->rowCount();

        if ($isValid) return true;
        return false;
    }

    /**
     * Ajoute un item dans la base de données
     *
     * @param string $name
     * @return bool
     */
    public function addItem($name): bool
    {
        $itemName = htmlspecialchars($name);
        $date = new DateTime('now');
        $createdAtDate = $date->format('Y-m-d H:i:s');

        $sql = "INSERT INTO items (items.name, items.createdAt) VALUES (:name, :createdAt);";
        $request = $this->_PDO->prepare($sql);
        $request->bindParam(':name', $itemName);
        $request->bindParam(':createdAt', $createdAtDate);
        $request->execute();

        $isValid = $request->rowCount();

        if ($isValid) return true;
        return false;
    }
}