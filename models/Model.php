<?php


namespace Model;

use PDO;
use PDOException;

class Model
{
    protected $_PDO;

    public function __construct()
    {
        $this->pdoConnection();
    }

    /**
     * Récupère les variables d'environnements (env.php)
     *
     * @return array
     */
    private function getEnv(): array
    {
        include 'env.php';

        if (!isset($BDD_HOST) || !isset($BDD_NAME) || !isset($BDD_USER) || !isset($BDD_PASS)) {
            die("Variables d'environnement non définies.");
        }

        return array(
            "host" => $BDD_HOST,
            "name" => $BDD_NAME,
            "user" => $BDD_USER,
            "pass" => $BDD_PASS
        );
    }

    /**
     * Connexion à la base de données
     */
    private function pdoConnection(): void
    {
        $ENVS = $this->getEnv();

        /**
         * Paramètres PDO
         */
        $BDD_OPTIONS = [
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
            PDO::MYSQL_ATTR_FOUND_ROWS   => true
        ];

        try {
            $this->_PDO = new PDO("mysql:host=" . $ENVS['host'] . ";dbname=". $ENVS['name'] .";charset=utf8mb4", $ENVS['user'], $ENVS['pass'], $BDD_OPTIONS);
        } catch (PDOException $exception) {
            $this->_PDO = null;
            die("Erreur !: " . $exception->getMessage() . "<br/>");
        }

        unset($ENVS);
    }
}