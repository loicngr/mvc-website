<?php
    use Controller\Controller;
    Controller::the_header('Todos');

    /** @var array $todosList */
?>

<div class="container">
    <h1>Todo List</h1>

    <h2>Add new ToDo</h2>
    <form action="/todo/add" method="post">

        <label> ToDo Name :
            <input type="text"  min="2" max="59" maxlength="59" name="name" placeholder="ToDo name" required>
        </label>

        <button type="submit">Add</button>
    </form>

    <h2>List</h2>
    <ul>
        <?php
            foreach ($todosList as $todo):
                $todoName = htmlspecialchars($todo->name);
                $todoId = intval($todo->id);

                ?>
                    <li>
                        <details>
                            <summary><?= $todoName ?></summary>
                            <form action="/todo/delete" method="post">
                                <input type="hidden" name="id" value="<?= $todoId ?>" readonly>

                                <button type="button"><a href="/todo/<?= $todoId ?>">Open</a></button>

                                <button type="submit">Delete</button>
                            </form>
                            <form action="/todo/update" method="post">

                                <label> ToDo Name :
                                    <input type="text" min="2" max="59" maxlength="59" name="name" placeholder="New toDo name" required>
                                </label>

                                <input type="hidden" name="id" value="<?= $todoId ?>" readonly>

                                <button type="submit">Update</button>
                            </form>
                        </details>
                    </li>
                <?php
            endforeach;
        ?>
    </ul>
</div>

<?php
    echo "<a href='/'>Back to home.</a>";

    Controller::the_footer();
?>