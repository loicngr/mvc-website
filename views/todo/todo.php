<?php
    use Controller\Controller;
    Controller::the_header('Todos');

    /** @var object $todo */
?>

<div class="container">
    <h1>Todo</h1>
    <h3><?= $todo->name ?></h3>
</div>

<?php
    echo "<a href='/todos'>Back.</a>";

    Controller::the_footer();
?>