<?php
    use Controller\Controller;
    Controller::the_header('Home page');

    echo "<h1>Welcome!</h1><br><a href='/todos'>ToDO list</a>";

    Controller::the_footer();