<?php
    /**
     * @var string $the_html_lang Html lang attr
     * @var string $the_title Page title
     */
?>
<!doctype html>
<html lang="<?= $the_html_lang ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title><?= $the_title ?></title>

    <style>
        body {
            display: grid;
            place-content: center;

            background-color: #e0e0e0;
        }

        h2 {
            margin-bottom: 20px;
        }
        h2::after {
            content: " ";
            height: 1px;
            width: 100%;

            display: block;

            background-color: silver;
        }
        ul {
            padding: 0;
        }
        ul li {
            list-style: none;
        }
        .container {
            padding: 20px;

            box-sizing: border-box;

            margin-bottom: 50px;
        }
    </style>
</head>
<body>