<?php

namespace View;

class View
{
    static function the_header($pageTitle)
    {
        $the_title = htmlspecialchars($pageTitle);
        $the_html_lang = 'en';

        include 'views/common/header.php';
    }

    static function the_footer()
    {
        include 'views/common/footer.php';
    }
}