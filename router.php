<?php

namespace Router;

include_once 'models/ToDoModel.php';
include_once 'utils.php';

use HomeController\HomeController;
use ToDoController\ToDoController;
use ToDoModel\ToDoModel;
use Utils\Utils;

class Router implements Utils
{
    private $routes;

    /** @var ToDoController $todoController */
    private $todoController;

    /** @var HomeController $homeController */
    private $homeController;

    protected function add_route($route, callable $closure): void
    {
        require_once 'controllers/ToDoController.php';
        require_once 'controllers/HomeController.php';

        $this->todoController = new ToDoController();
        $this->homeController = new HomeController();
        $this->routes[$route] = $closure;
    }

    protected function addToDoByIDRoutes(): void
    {
        $todoModel = new ToDoModel();
        $todosList = $todoModel->getItems();

        foreach ($todosList as $todo):
            $baseUrl = '/todo/'. $todo->id;
            $this->add_route($baseUrl, array($this, 'todoView'));
        endforeach;
    }

    protected function execute(): void
    {
        $path = $_SERVER['REQUEST_URI'];

        // Si la route demandé existe bien ?
        if(array_key_exists($path, $this->routes)) {
            // On affiche la route demandé
            $this->routes[$path]();
        } else {
            // On affiche la route par défaut
            $this->routes['/']();
        }
    }

    /**
     * Page d'accueil
     */
    private function homeView(): void
    {
        $this->homeController->render('homeView');
    }

    /**
     * Page des ToDos
     */
    private function todosView(): void
    {
        $this->todoController->render('todo/todos');
    }

    /**
     * Page d'un to do
     */
    private function todoView(): void
    {
        $path = $_SERVER['REQUEST_URI'];

        if(!array_key_exists($path, $this->routes)) {
            $this->printError("Page not found.");
        }

        $todoId = explode('/', $path);

        if (!key_exists(2, $todoId)) {
            $this->printError("Page not found.");
        }

        $todoId = intval($todoId[2]);
        $this->todoController->render('todo/todo', $todoId);
    }

    /**
     * Page pour mettre à jour des todos
     */
    private function todoUpdateView(): void
    {
        if (empty($_POST) || !isset($_POST['id']) || !is_numeric($_POST['id'])) $this->printError("ToDo Id not found.");
        if (empty($_POST) || !isset($_POST['name']) || !is_string($_POST['name'])) $this->printError("ToDo name not found.");

        $todoId = intval($_POST['id']);
        $todoName = strip_tags($_POST['name']);

        if ($this->todoController->update($todoId, $todoName)) {
            $todoPath = "/todos";
            header("Location: $todoPath");
        } else {
            $this->printError("ToDo not updated.");
        }
    }

    /**
     * Page pour ajouter un to do
     */
    private function todoAddView(): void
    {
        if (empty($_POST) || !isset($_POST['name']) || !is_string($_POST['name'])) $this->printError("ToDo name not found.");

        $todoName = strip_tags($_POST['name']);

        if ($this->todoController->add($todoName)) {
            $todoPath = "/todos";
            header("Location: $todoPath");
        } else {
            $this->printError("ToDo not added.");
        }
    }

    /**
     * Page pour supprimer un to do
     */
    private function todoDeleteView(): void
    {
        if (empty($_POST) || !isset($_POST['id']) || !is_numeric($_POST['id'])) $this->printError("ToDo Id not found.");

        $todoId = intval($_POST['id']);

        if ($this->todoController->delete($todoId)) {
            $todoPath = "/todos";
            header("Location: $todoPath");
        } else {
            $this->printError("ToDo not deleted.");
        }
    }

    /**
     * Affiche une erreur dans le page
     *
     * @param string $msg
     */
    public function printError($msg): void
    {
        printf($msg);
        echo "<br><a href='/'>Back home</a>.";
        die();
    }
}