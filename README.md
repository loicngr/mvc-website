# MVC website

### Start server
- To start server use ``php -S localhost:8000``

### Env variables
- create new ``env.php`` file in ``models`` folder and set this variables : 
```php
<?php

    $BDD_HOST = "localhost";
    $BDD_NAME = "todoList";
    $BDD_USER = "";
    $BDD_PASS = "";
```